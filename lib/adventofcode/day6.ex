defmodule Adventofcode.Day6 do
  @moduledoc """
  http://adventofcode.com/2017/day/6
  """
  def answer(banks) do
    {redistributions, _duplicate_banks} = count_redistributions(banks, [banks])

    Enum.count(redistributions) - 1
  end

  def part2_answer(banks) do
    {redistributions, duplicate_banks} = count_redistributions(banks, [banks])
    start = Enum.find_index(redistributions, fn banks -> banks == duplicate_banks end)
    finish = Enum.find_index(List.replace_at(redistributions, start, []), fn banks -> banks == duplicate_banks end)

    finish - start
  end

  def solution do
    banks = [0, 5, 10, 0, 11, 14, 13, 4, 11, 8, 8, 7, 1, 4, 12, 11]
    answer(banks)
  end

  def part2_solution do
    banks = [0, 5, 10, 0, 11, 14, 13, 4, 11, 8, 8, 7, 1, 4, 12, 11]
    part2_answer(banks)
  end

  defp count_redistributions(banks, redistributions) do
    max_num = Enum.max(banks)
    max_num_index = Enum.find_index(banks, fn num -> num == max_num end)
    new_banks = List.replace_at(banks, max_num_index, 0)
    new_banks = redistribute(new_banks, max_num, max_num_index + 1)
    new_redistributions = redistributions ++ [new_banks]

    if Enum.find_index(redistributions, fn banks -> new_banks == banks end) != nil do
      {new_redistributions, new_banks}
    else
      count_redistributions(new_banks, new_redistributions)
    end
  end

  defp redistribute(banks, amount, _index) when amount <= 0 do
    banks
  end

  defp redistribute(banks, amount, index) do
    if Enum.at(banks, index) == nil do
      new_banks = List.replace_at(banks, 0, Enum.at(banks, 0) + 1)

      redistribute(new_banks, amount - 1, 1)
    else
      bank_value = Enum.at(banks, index)
      new_banks = List.replace_at(banks, index, bank_value + 1)
      redistribute(new_banks, amount - 1, index + 1)
    end
  end
end
