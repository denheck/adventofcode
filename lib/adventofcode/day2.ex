defmodule Adventofcode.Day2 do
  @moduledoc """
  http://adventofcode.com/2017/day/2
  """
  def answer(input) do
    ints = format_input(input)

    Enum.reduce ints, 0, fn
      ls, acc -> acc + (Enum.max(ls) - Enum.min(ls))
    end
  end

  def part2_answer(input) do
    ints = format_input(input)

    Enum.sum(Enum.map ints, &remainderless_divide_quotient/1)
  end

  def solution do
   {:ok, input} = File.read("files/day2.txt")
    answer(input)
  end

  def part2_solution do
   {:ok, input} = File.read("files/day2.txt")
    part2_answer(input)
  end

  defp remainderless_divide_quotient(ints) do
    Enum.reduce ints, nil, fn
      i, acc -> Enum.reduce ints, acc, fn
        j, acc when i != j -> if rem(i, j) == 0 do
          div(i, j)
        else
          acc
        end
        _, acc -> acc
      end
    end
  end

  defp format_input(input) do
    ints = String.split(String.trim(input), "\n")
    ints = Enum.map(ints, fn i -> String.split(i, "\t") end)

    Enum.map(ints, fn ls -> Enum.map(ls, fn i -> String.to_integer(i) end) end)
  end
end
