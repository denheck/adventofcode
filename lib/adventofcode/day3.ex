defmodule Adventofcode.Day3 do
  @moduledoc """
  http://adventofcode.com/2017/day/3
  """
  def answer(input) do
    List.last Enum.take(stream(), input)
  end

  def solution do
    answer(265149)
  end

  @doc """
  pattern = [
    1, # start
    1, # last
    2, # -1 + -2
    4, # -1 + -2 + -3
    5, # -1 + -4
    10, # -1 + -2 + -5
    11, # -1 + -5
    23, # -1 + -2 + -7
    25, # -1 + -7 + -8
    26, # -1 + -8
    54, # -1 + -2 + -9
    57, # -1 + -9 + -10
    59, # -1 + -10 + -11
    122, # -1 + -2 + -11
    133, # -1 + -11 + -12
    142, # -1 + -11 + -12
    147
  ]
  """
  def stream2 do

  end

  @doc """
    pattern = [0,
               1, 2,
               1, 2,
               1, 2,
               1, 2,
               3, 2, 3, 4,
               3, 2, 3, 4,
               3, 2, 3, 4,
               3, 2, 3, 4,
               5, 4, 3, 4, 5, 6,
               5, 4, 3, 4, 5, 6,
               5, 4, 3, 4, 5, 6,
               5, 4, 3, 4, 5, 6,
               7, 6, 5, 4, 5, 6, 7, 8,
               7, 6, 5, 4, 5, 6, 7, 8,
               7, 6, 5, 4, 5, 6, 7, 8,
               7, 6, 5, 4, 5, 6, 7, 8]

  """
  def stream do
      Stream.concat(
        [0, 1, 2, 1, 2, 1, 2, 1, 2],
        Stream.transform(Stream.cycle([nil]), {2, 4, 1}, fn
          _, {first, last, i} when rem(i, 4) == 0 -> {
            Enum.to_list(last-1..first+1) ++ Enum.to_list(first..last),
            {first + 1, last + 2, i + 1}
            }
          _, {first, last, i} -> {
            Enum.to_list(last-1..first+1) ++ Enum.to_list(first..last),
            {first, last, i + 1}
            }
        end)
      )
  end
end
