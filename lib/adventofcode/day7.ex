defmodule Adventofcode.Day7 do
  @moduledoc """
  http://adventofcode.com/2017/day/7
  """
  def answer(input) do
    common_ancestor_name(format(input))
  end

  def part2_answer(input) do
    programs = format(input)
    ancestor_name = common_ancestor_name(programs)

    imbalanced_program(programs, ancestor_name)
  end

  def solution do
    {:ok, input} = File.read("files/day7.txt")
    answer(input)
  end

  defp imbalanced_program(programs, program_name) do
    {_weight, children} = programs[program_name]
    child_weights = Enum.group_by(children, fn child_name -> total_weight(programs, programs[child_name]) end)
    imbalanced_children = Enum.find(child_weights, nil, fn {_weight, children} -> Enum.count(children) == 1 end)
    imbalanced_child = List.first(elem(imbalanced_children, 1))

    if imbalanced_child == nil do
      imbalanced_child
    else
      imbalanced_program(programs, imbalanced_child)
    end
  end

  defp total_weight(programs, {weight, children}) do
    weight + (children
      |> Enum.map(fn child -> elem(programs[child], 0) end)
      |> Enum.reduce(0, &(&1 + &2)))
  end

  defp common_ancestor_name(programs) do
    parents = Enum.filter(programs, fn {_name, {_weight, children}} -> Enum.count(children) > 0 end)
    child_names = Enum.dedup(Enum.flat_map(parents, fn {_name, {_weight, children}} -> children end))
    parent_names = Enum.map(parents, fn {name, {_weight, _children}} -> name end)

    List.first(parent_names -- child_names)
  end

  defp format(input) do
    input
      |> String.trim
      |> String.split("\n")
      |> Enum.map(&format_program/1)
      |> Enum.into(%{})
  end

  defp format_program(input) do
    program = input |> String.split("->") |> Enum.map(&String.trim/1)
    [name, weight] = String.split(List.first(program))
    {weight, ""} = weight |> String.trim_leading("(") |> String.trim_trailing(")") |> Integer.parse
    children = Enum.at(program, 1, "")
      |> String.split(",")
      |> Enum.map(&String.trim/1)
      |> Enum.filter(&(&1 != ""))

    {name, {weight, children}}
  end
end
