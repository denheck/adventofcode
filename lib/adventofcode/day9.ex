defmodule Adventofcode.Day9 do
  @moduledoc """
  http://adventofcode.com/2017/day/9
  """
  def answer(input) do
    state = %{
      :score => 0,
      :garbage_characters => 0,
      :group_depth => 0,
      :in_garbage => false,
      :cancel_next_token => false
    }

    input
    |> format
    |> Enum.reduce(state, &interpret/2)
  end

  def solution do
    {:ok, input} = File.read("files/day9.txt")
    answer(input)
  end

  defp format(input) do
    input
    |> String.trim
    |> String.graphemes
  end

  defp interpret(token, state) do
    %{
      :score => score,
      :garbage_characters => garbage_characters,
      :group_depth => group_depth,
      :in_garbage => in_garbage,
      :cancel_next_token => cancel_next_token
    } = state

    if in_garbage do
      if cancel_next_token do
        %{state | :cancel_next_token => false}
      else
        case token do
          "!" -> %{state | :cancel_next_token => true}
          ">" -> %{state | :in_garbage => false}
          _ -> %{state | :garbage_characters => garbage_characters + 1}
        end
      end
    else
      case token do
        "{" -> %{state | :group_depth => group_depth + 1}
        "}" -> %{state | :score => score + group_depth, :group_depth => max(0, group_depth - 1)}
        "<" -> %{state | :in_garbage => true}
        _ -> state
      end
    end
  end
end
