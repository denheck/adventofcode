defmodule Adventofcode.Day4 do
  @moduledoc """
  http://adventofcode.com/2017/day/4
  """
  def answer(input) do
    Enum.sum(Enum.map(format(input), fn words -> if !has_duplicates(words), do: 1, else: 0 end))
  end

  def part2_answer(input) do
    Enum.sum(Enum.map(format(input), fn words -> if !has_anagram_duplicates(words), do: 1, else: 0 end))
  end

  def solution do
    {:ok, input} = File.read("files/day4.txt")
    answer(input)
  end

  def part2_solution do
    {:ok, input} = File.read("files/day4.txt")
    part2_answer(input)
  end

  defp format(input) do
    Enum.map(String.split(String.trim(input), "\n"), &String.split(&1))
  end

  def has_duplicates(words) do
    Enum.any?(Map.to_list(Enum.group_by(words, fn a -> a end)), fn {_word, word_list} -> Enum.count(word_list) > 1 end)
  end

  def has_anagram_duplicates(words) do
    sorted_words = Enum.map(words, fn word -> Enum.join(Enum.sort(String.graphemes(word))) end)
    Enum.any?(Map.to_list(Enum.group_by(sorted_words, fn a -> a end)), fn {_word, word_list} -> Enum.count(word_list) > 1 end)
  end
end
