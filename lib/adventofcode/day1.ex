defmodule Adventofcode.Day1 do
  @moduledoc """
  http://adventofcode.com/2017/day/1
  """
  def answer(input) do
    ints = input |> String.trim |> String.graphemes
    ints = Enum.map(ints, &String.to_integer/1)
    {sum, _} = Enum.reduce ints, {0, nil}, fn
      next, {acc, prev} when prev == next -> {acc + next, next}
      next, {acc, _} -> {acc, next}
    end

    if List.first(ints) == List.last(ints) do
      sum + List.first(ints)
    else
      sum
    end
  end

  def part2_answer(input) do
    ints = input |> String.trim |> String.graphemes
    ints = Enum.map(ints, &String.to_integer/1)
    {first_list, second_list} = Enum.split(ints, round(Enum.count(ints) / 2))

    sum_halfway(first_list, second_list, 0)
  end

  def sum_halfway([hd1 | tl1], [hd2 | tl2], acc) when hd1 == hd2 do
    sum_halfway(tl1, tl2, hd1 + hd2 + acc)
  end

  def sum_halfway([_ | tl1], [_ | tl2], acc) do
    sum_halfway(tl1, tl2, acc)
  end

  def sum_halfway([], [], acc) do
    acc
  end

  def solution do
    {:ok, input} = File.read("files/day1.txt")
    answer(input)
  end

  def part2_solution do
    {:ok, input} = File.read("files/day1.txt")
    part2_answer(input)
  end
end
