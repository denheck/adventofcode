defmodule Adventofcode.Day5 do
  @moduledoc """
  http://adventofcode.com/2017/day/5
  """
  def answer(input) do
    offsets = format(input)
    jump(0, 0, offsets)
  end

  def part2_answer(input) do
    offsets = format(input)
    jump2(0, 0, offsets)
  end

  def solution do
    {:ok, input} = File.read("files/day5.txt")
    answer(input)
  end

  def part2_solution do
    {:ok, input} = File.read("files/day5.txt")
    part2_answer(input)
  end

  defp jump2(steps, position, offsets) do
    if position < 0 || position >= Enum.count(offsets) do
      steps
    else
      offset = Enum.at(offsets, position)
      new_position = position + offset
      new_offset = if offset >= 3, do: offset - 1, else: offset + 1
      new_steps = steps + 1
      new_offsets = List.replace_at(offsets, position, new_offset)

      jump2(new_steps, new_position, new_offsets)
    end
  end

  defp jump(steps, position, offsets) do
    if position < 0 || position >= Enum.count(offsets) do
      steps
    else
      offset = Enum.at(offsets, position)
      new_position = position + offset
      new_offset = offset + 1
      new_steps = steps + 1
      new_offsets = List.replace_at(offsets, position, new_offset)

      jump(new_steps, new_position, new_offsets)
    end
  end

  defp format(input) do
    input
    |> String.split
    |> Enum.map(&String.to_integer/1)
  end
end
