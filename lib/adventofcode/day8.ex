defmodule Adventofcode.Day8 do
  @moduledoc """
  http://adventofcode.com/2017/day/8
  """
  def answer(input) do
    registers = %{}

    input
    |> format
    |> Enum.map(&tokenize/1)
    |> Enum.reduce({0, registers}, &interpret/2)
    |> elem(1)
    |> Enum.sort(fn {_r1, a1}, {_r2, a2} -> a1 >= a2 end)
    |> List.first
    |> elem(1)
  end

  def part2_answer(input) do
    registers = %{}

    input
    |> format
    |> Enum.map(&tokenize/1)
    |> Enum.reduce({0, registers}, &interpret/2)
    |> elem(0)
  end

  def solution do
    {:ok, input} = File.read("files/day8.txt")
    answer(input)
  end

  def part2_solution do
    {:ok, input} = File.read("files/day8.txt")
    part2_answer(input)
  end

  defp format(input) do
    input
      |> String.trim
      |> String.split("\n")
  end

  defp tokenize(line) do
    String.split(line, " ")
  end

  defp interpret(tokens, {max, registers}) do
    [register, command, amount, "if", condition_register, condition_modifier, condition_amount] = tokens
    amount = String.to_integer(amount)
    condition_amount = String.to_integer(condition_amount)
    registers = if Map.has_key?(registers, register) do
      registers
    else
      Map.put(registers, register, 0)
    end
    registers = if Map.has_key?(registers, condition_register) do
      registers
    else
      Map.put(registers, condition_register, 0)
    end

    if condition_true(registers, condition_register, condition_modifier, condition_amount) do
      register_value = if command == "inc" do
        registers[register] + amount
      else
        registers[register] - amount
      end

      {max(register_value, max), Map.put(registers, register, register_value)}
    else
      {max, registers}
    end
  end

  defp condition_true(registers, condition_register, condition_modifier, condition_amount) do
    case condition_modifier do
      ">" ->
        registers[condition_register] > condition_amount
      "<" ->
        registers[condition_register] < condition_amount
      ">=" ->
        registers[condition_register] >= condition_amount
      "<=" ->
        registers[condition_register] <= condition_amount
      "!=" ->
        registers[condition_register] != condition_amount
      "==" ->
        registers[condition_register] == condition_amount
    end
  end
end
